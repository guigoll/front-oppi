import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PortalComponent } from './layout/portal/portal.component';
import { HomeComponent } from './portal/home/home.component';
import { LoginComponent } from './portal/login/login.component';
import { SignupComponent } from './portal/signup/signup.component';

import { ROUTES } from './routes';

@NgModule({
    declarations: [
        PortalComponent,
        HomeComponent,
        LoginComponent,
        SignupComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(ROUTES),
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule

    ],
    exports: [
        RouterModule
    ],
    providers: [
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
})
export class RouteModule { }

