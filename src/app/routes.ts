
import { Routes } from '@angular/router';

import { PortalComponent } from './layout/portal/portal.component';
import { LoginComponent } from './portal/login/login.component';
import { SignupComponent } from './portal/signup/signup.component';
import { HomeComponent } from './portal/home/home.component';

import { AdminComponent } from './layout/admin/admin.component';
import { DashComponent } from './admin/dash/dash.component';

export const ROUTES: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    {
        path: '', component: PortalComponent,
        children: [
            { path: 'home', component: HomeComponent },
            { path: 'login', component: LoginComponent },
            { path: 'signup', component: SignupComponent }
        ]
    },
    {
        path: 'admin', component: AdminComponent,
        children: [
            { path: '', component: DashComponent }
        ]
    }
]
