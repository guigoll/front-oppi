import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JumbotronAdvertsComponent } from './jumbotron-adverts.component';

describe('JumbotronAdvertsComponent', () => {
  let component: JumbotronAdvertsComponent;
  let fixture: ComponentFixture<JumbotronAdvertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JumbotronAdvertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JumbotronAdvertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
