import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carrossel',
  templateUrl: './carrossel.component.html',
  styleUrls: ['./carrossel.component.css']
})
export class CarrosselComponent implements OnInit {

  constructor() { }

  images = [1, 2, 3].map(() => `https://picsum.photos/2498/364?blur=2&random&t=${Math.random()}`)

  ngOnInit() {

  }

}
