import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PortalComponent } from './layout/portal/portal.component'
import { NavbarComponent } from './portal/components/navbar/navbar.component';
import { JumbotronComponent } from './portal/components/jumbotron/jumbotron.component';
import { CarrosselComponent } from './portal/components/carrossel/carrossel.component';
import { JumbotronAdvertsComponent } from './portal/components/jumbotron-adverts/jumbotron-adverts.component';
import { SignupComponent } from './portal/signup/signup.component';
import { HomeComponent } from './portal/home/home.component';
import { LoginComponent } from './portal/login/login.component';

import { AdminComponent } from './layout/admin/admin.component';
import { HeaderComponent } from './layout/admin/components/header/header.component';
import { SidenavComponent } from './layout/admin/components/sidenav/sidenav.component';
import { MainComponent } from './layout/admin/components/main/main.component';
import { DashComponent } from './admin/dash/dash.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    JumbotronComponent,
    CarrosselComponent,
    JumbotronAdvertsComponent,
    SignupComponent,
    HomeComponent,
    LoginComponent,
    DashComponent,
    AdminComponent,
    HeaderComponent,
    SidenavComponent,
    MainComponent,
    PortalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
