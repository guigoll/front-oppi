import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes } from '@angular/router';

import { AdminComponent } from '../layout/admin/admin.component';

export const ROUTES: Routes = [

]

@NgModule({
    declarations: [
        AdminComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(ROUTES),
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule
    ],
    exports: [
        RouterModule
    ],
    providers: [

    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
})
export class RouteModule { }
